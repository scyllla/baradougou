<?php

namespace App\Data;

class SearchData
{
    /**
     * @var string
     */
    public $q='';

    /**
     * @var Category
     */
    public $categories;

    /**
     * @var Localite
     */
    public $localite;

    /**
     * @var null|integer
     */
    public $max;

    /**
     * @var null|integer
     */
    public $min;

    /**
     * @var boolean
     */
    public $promo = false;
}