<?php

namespace App\Controller;

use App\Entity\Prestation;
use App\Entity\Souscription;
use App\Form\SouscriptionType;
use App\Repository\SouscriptionRepository;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/souscription")
 */
class SouscriptionController extends AbstractController
{
    /**
     * @Route("/", name="souscription_index", methods={"GET"})
     */
    public function index(SouscriptionRepository $souscriptionRepository): Response
    {
        return $this->render('souscription/index.html.twig', [
            'souscriptions' => $souscriptionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{id}", name="souscription_new", methods={"GET","POST"})
     */
    public function new(Prestation $prestation,Request $request,MailerInterface $mailer): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $souscription = new Souscription();
        $form = $this->createForm(SouscriptionType::class, $souscription);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $souscription->setPrestation($prestation);
            $souscription->setUser($user);
            $souscription->setEtat('Ouvert');
            $entityManager->persist($souscription);
            $entityManager->flush();

            $message = (new TemplatedEmail())
                    ->from('contact@baradougou.com')
                    ->to($user->getEmail())
                    ->subject('Confirmation de commande')
                    ->htmlTemplate('souscription/mail.html.twig')

                    // pass variables (name => value) to the template
                    ->context([
                        'user' => $user,
                        'service' => $prestation,

                    ])
                   
                    /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'emails/registration.txt.twig',
                    ['name' => $name]
                ),
                'text/plain'
            )
            */;


            
            try{
                $mailer->send($message);
                }catch(Exception $e){
                }

            $email = (new TemplatedEmail())
            ->from('contact@baradougou.com')
            ->to('seta.0701@gmail.com',
            'diakitescylla@gmail.com',
            'farimbodj@gmail.com'
            )
            ->subject('Notification')
            ->htmlTemplate('souscription/mail-admin.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'user' => $user,
                'service' => $prestation,
                'souscription' => $souscription

            ]);

            
            try{
                $mailer->send($email);
                }catch(Exception $e){
                }

            return $this->redirectToRoute('souscription_success');
        }

        return $this->render('souscription/new.html.twig', [
            'souscription' => $souscription,
            'prestation'=> $prestation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="souscription_show", methods={"GET"})
     */
    public function show(Souscription $souscription): Response
    {
        return $this->render('souscription/show.html.twig', [
            'souscription' => $souscription,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="souscription_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Souscription $souscription): Response
    {
        $form = $this->createForm(SouscriptionType::class, $souscription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('souscription_index');
        }

        return $this->render('souscription/edit.html.twig', [
            'souscription' => $souscription,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="souscription_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Souscription $souscription): Response
    {
        if ($this->isCsrfTokenValid('delete'.$souscription->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($souscription);
            $entityManager->flush();
        }

        return $this->redirectToRoute('souscription_index');
    }
}
