<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Avis;
use App\Entity\Newsletter;
use App\Entity\Prestation;
use App\Entity\Souscription;
use App\Form\AvisType;
use App\Form\ContactType;
use App\Form\SearchForm;
use App\Repository\AvisRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoryRepository;
use App\Repository\FavoritsRepository;
use App\Repository\GalleryRepository;
use App\Repository\LocaliteRepository;
use App\Repository\PrestationRepository;
use App\Repository\SouscriptionRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(PrestationRepository $prestationRepository, CategoryRepository $categoryRepository, LocaliteRepository $localiteRepository, Request $request, AvisRepository $avisRepository, UserRepository $userRepository, GalleryRepository $galleryRepository)
    {
        $data = new SearchData();
        $form = $this->createForm(SearchForm::class, $data);
        $form->handleRequest($request);
        // dd($avisRepository->findAll());
        
        $prestations = $prestationRepository->findSearch($data);
        return $this->render('default/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
            'avis' => $avisRepository->findAll(),
            'localites' => $localiteRepository->findAll(),
            'form' => $form->createView(),
            'prestataire' => $userRepository->findByProfil('prestataire'),
            'prestations' => $prestationRepository->findBy(array(), array('id' => 'DESC')),
            'galerie' => $galleryRepository->findBy(array(), array('id' => 'DESC'))
        ]);
    }

    /**
     * @Route("/success", name="souscription_success", methods={"GET"})
     */
    public function success(): Response
    {
        return $this->render('default/success.html.twig');
    }

    /**
     * @Route("/success/account", name="insscription_success", methods={"GET"})
     */
    public function successInscription(): Response
    {
        return $this->render('registration/success.html.twig');
    }

    public function rechercheindex(Request $request, PrestationRepository $prestationRepository, CategoryRepository $categoryRepository, LocaliteRepository $localiteRepository): Response
    {
        $data = new SearchData();
        $form = $this->createForm(SearchForm::class, $data);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->render('prestation/index.html.twig', [
                'prestations' => $prestationRepository->findSearch($data),
            ]);
        }
        return $this->render('default/recherche.html.twig', [
            'categories' => $categoryRepository->findAll(),
            'localites' => $localiteRepository->findAll(),
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/rech", name="prestation_rech")
     */
    public function rech(PrestationRepository $prestationRepository, PaginatorInterface $paginator, Request $request, CategoryRepository $categoryRepository): Response
    {
        $search = new SearchData();
        $form = $this->createForm(SearchForm::class, $search);
        $form->handleRequest($request);
        $query = null;


        $query = $prestationRepository->createQueryBuilder('p')
            ->select('s', 'l', 'p')
            ->join('p.service', 's')
            ->join('p.localite', 'l');
        if (!empty($request->query->get('rech'))) {
            $query = $query
                ->andWhere('p.titre LIKE :q')
                ->setParameter('q', "%{$request->query->get('rech')}%");
        }

       

        if (!empty($request->query->get('order'))) {
            
            if($request->query->get('order') == 'low')
            $query = $query
            ->orderBy('p.price', 'ASC');
            else
            $query = $query
            ->orderBy('p.price', 'DESC');

        }
        if (!empty($request->query->get('categorie'))) {
            $query = $query
                ->andWhere('s.id IN (:service)')
                ->setParameter('service', $request->query->get('categorie'));
        }

        if (!empty($request->query->get('localite'))) {
            $query = $query
                ->andWhere('l.id IN (:localite)')
                ->setParameter('localite', $request->query->get('localite'));
        }
        // Paginate the results of the query
        $prestations = $paginator->paginate(
            // Doctrine Query, not results
            $query,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            20
        );
        return $this->render('prestation/index.html.twig', [
            'prestations' => $prestations,
            'form' => $form->createView(),
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request,MailerInterface $mailer)
    {$form = $this->createForm(ContactType::class,null);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        
            $email = (new Email())
            ->from('contact@baradougou.com')
            ->to('seta.0701@gmail.com')
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject("Baradougou website client ". $data["subject"])
            ->text($data["message"]."\nContactMail :".$data["email"]);
            //dump($email);die();

            $mailer->send($email);

                return $this->redirectToRoute('contact', [
                    'massage' => 'Votre message a été evoyé merci',
                ]);
            }
        return $this->render('default/contact.html.twig', [
            'form' => $form->createView(),
            'massage' => '',
        ]);
    }

       /**
     * @Route("/a/propos", name="about")
     */
    public function about()
    {
        return $this->render('default/apropos.html.twig');
    }

    /**
     * @Route("condition-utilisation", name="condition")
     */
    public function condition()
    {   
        return$this->render('default/disclaimer.html.twig');
    }
     /**
     * @Route("condition-utilisation-arabe", name="condition-arabe")
     */

    public function conditionAr()
    {   
        return$this->render('default/disclaimer-ar.html.twig');
    }

    /**
     * @Route("/categories", name="categories")
     */
    public function categories()
    {
        return $this->render('default/categories.html.twig');
    }

    /**
     * @Route("/realisations", name="realisation")
     */
    public function realisation(GalleryRepository $galleryRepository)
    {
        return $this->render('default/blog-left-sideba.html.twig',[
            'realisations'=>$galleryRepository->findBy(array(), array('id' => 'DESC'))

        ]);
    }
    /**
     * @Route("/compte", name="compte")
     */
    public function compte()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('default/compte.html.twig');
    }

    /**
     * @Route("/avis/{id}", name="avis")
     */
    public function avis(Souscription $prestation, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $avis = new Avis();
        $form = $this->createForm(AvisType::class, $avis);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $avis->setSouscription($prestation)->setUser($user);
            $entityManager->persist($avis);
            $entityManager->flush();

            return $this->redirectToRoute('commande');
        }
        return $this->render('default/avis.html.twig', [
            'prestation' => $prestation,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/annonces", name="annonces")
     */
    public function annonces(SouscriptionRepository $souscriptionRepository)
    {
        $this->denyAccessUnlessGranted('ROLE_PRESTATAIRE');

        $user = $this->getUser();
        // dd($user->getService());
        return $this->render('default/annonces.html.twig', [
            'souscriptions' => $souscriptionRepository->findBy(array('etat' => 'Ouvert')),
        ]);
    }

    /**
     * @Route("/engagements", name="engagement_list")
     */
    public function engagements(SouscriptionRepository $souscriptionRepository)
    {
        $this->denyAccessUnlessGranted('ROLE_PRESTATAIRE');

        $user = $this->getUser();
        return $this->render('default/engagement.html.twig', [
            'souscriptions' => $souscriptionRepository->findByPrestataire($user)
        ]);
    }

    /**
     * @Route("/{id}/engager", name="engager", methods={"GET","POST"})
     */
    public function engager(Request $request, Souscription $souscription): Response
    {
        $this->denyAccessUnlessGranted('ROLE_PRESTATAIRE');

        $user = $this->getUser();

        $souscription->setPrestataire($user);
        $souscription->setEtat('En cours');
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('annonces');
    }

    /**
     * @Route("/favoris", name="favorit")
     */
    public function favorit(FavoritsRepository $favoritsRepository)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        return $this->render('default/favorit.html.twig', [
            'favorits' => $favoritsRepository->findByUser($user),
            ]);
    }

    /**
     * @Route("/archives", name="archive", methods={"GET"}): Response
     */
    public function archive(SouscriptionRepository $souscriptionRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        ($souscriptionRepository->findByUser($user));
        return $this->render('default/archive.html.twig', [
            'souscriptions' => $souscriptionRepository->findByUser($user),
        ]);
    }

    /**
     * @Route("/commandes", name="commande", methods={"GET"}): Response
     */
    public function commande(SouscriptionRepository $souscriptionRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        ($souscriptionRepository->findByUser($user));
        return $this->render('default/commande.html.twig', [
            'souscriptions' => $souscriptionRepository->findByUser($user),
        ]);
    }



    /**
     * @Route("/postuler", name="postuler")
     */
    public function postuler()
    {
        return $this->render('default/postuler.html.twig');
    }

    /**
     * @Route("/faq", name="faq")
     */
    public function faq()
    {
        return $this->render('default/faq.html.twig');
    }

    /**
     * @Route("/newletter", name="newslatters_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $newslatter = new Newsletter();

        if ($request->query->get('email') != null) {
            $newslatter->setEmail($request->query->get('email'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newslatter);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }
    }
}
