<?php

namespace App\Controller;

use App\Entity\Favorits;
use App\Entity\Prestation;
use App\Form\FavoritsType;
use App\Repository\FavoritsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/favorits")
 */
class FavoritsController extends AbstractController
{
    

    /**
     * @Route("/new/{id}", name="favorits_new", methods={"GET","POST"})
     */
    public function new(Prestation $prestation, Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $favorit = new Favorits();
        $favorit->setPrestation($prestation);
        $favorit->setUser($user);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($favorit);
        $entityManager->flush();

        return $this->redirectToRoute('favorit');
    }

    /**
     * @Route("/{id}", name="favorits_show", methods={"GET"})
     */
    public function show(Favorits $favorit): Response
    {
        return $this->render('favorits/show.html.twig', [
            'favorit' => $favorit,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="favorits_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Favorits $favorit): Response
    {
        $form = $this->createForm(FavoritsType::class, $favorit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('favorits_index');
        }

        return $this->render('favorits/edit.html.twig', [
            'favorit' => $favorit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="favorits_delete", methods={"GET"})
     */
    public function delete(Request $request, Favorits $favorit): Response
    {
        
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($favorit);
            $entityManager->flush();
    

        return $this->redirectToRoute('favorit');
    }
}
