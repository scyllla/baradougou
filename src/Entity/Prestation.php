<?php

namespace App\Entity;

use App\Repository\PrestationRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=PrestationRepository::class)
 */
class Prestation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\ManyToOne(targetEntity=Service::class, inversedBy="prestations", cascade={"remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $service;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePub;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $click;

    /**
     * @ORM\ManyToOne(targetEntity=Localite::class, inversedBy="prestations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $localite;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Image", cascade = {"persist"})
     */ 
    private $images;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    

    /**
     * @ORM\OneToMany(targetEntity=Souscription::class, mappedBy="prestation", cascade={"remove"})
     */
    private $souscription;

    /**
     * @ORM\OneToMany(targetEntity=Favorits::class, mappedBy="prestation", cascade={"remove"})
     */
    private $favorits;

    

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->datePub = new \DateTime();
        $this->souscription = new ArrayCollection();
        $this->favorits = new ArrayCollection();
        $this->price = 150;
        
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDatePub(): ?\DateTimeInterface
    {
        return $this->datePub;
    }

    public function setDatePub(\DateTimeInterface $datePub): self
    {
        $this->datePub = $datePub;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getClick(): ?int
    {
        return $this->click;
    }

    public function setClick(?int $click): self
    {
        $this->click = $click;

        return $this;
    }

    public function getLocalite(): ?Localite
    {
        return $this->localite;
    }

    public function setLocalite(?Localite $localite): self
    {
        $this->localite = $localite;

        return $this;
    }

     /**
     * @return Collection|Image[]
     */
    public function getImages() 
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
        }


        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
        }

        return $this;
    }

    public function __toString(){
        return $this->titre;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    

    /**
     * @return Collection|Souscription[]
     */
    public function getSouscription(): Collection
    {
        return $this->souscription;
    }

    public function addSouscription(Souscription $souscription): self
    {
        if (!$this->souscription->contains($souscription)) {
            $this->souscription[] = $souscription;
            $souscription->setPrestation($this);
        }

        return $this;
    }

    public function removeSouscription(Souscription $souscription): self
    {
        if ($this->souscription->contains($souscription)) {
            $this->souscription->removeElement($souscription);
            // set the owning side to null (unless already changed)
            if ($souscription->getPrestation() === $this) {
                $souscription->setPrestation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Favorits[]
     */
    public function getFavorits(): Collection
    {
        return $this->favorits;
    }

    public function addFavorit(Favorits $favorit): self
    {
        if (!$this->favorits->contains($favorit)) {
            $this->favorits[] = $favorit;
            $favorit->setPrestation($this);
        }

        return $this;
    }

    public function removeFavorit(Favorits $favorit): self
    {
        if ($this->favorits->contains($favorit)) {
            $this->favorits->removeElement($favorit);
            // set the owning side to null (unless already changed)
            if ($favorit->getPrestation() === $this) {
                $favorit->setPrestation(null);
            }
        }

        return $this;
    }

   

    
}
