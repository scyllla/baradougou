var newproducts = $("#testimonial-item");
  newproducts.owlCarousel({
    autoplay: true,
    nav: false,
    autoplayHoverPause:true,
    smartSpeed: 350,
    dots: true,
    margin: 10,
    loop: true,
    navText: [
        <i class="fas fa-angle-left"></i>,
        <i class="fas fa-angle-right"></i>
    ],
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
        },
        575: {
            items: 2,
        },
        991: {
            items: 2,
        }
      }
  });